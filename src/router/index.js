import Vue from 'vue'
import Router from 'vue-router'
import Haidomo from '@/components/Haidomo'
import Route from '@/components/Route'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Haidomo',
      component: Haidomo
    },
    {
      path: '*',
      // redirect: '/'
      name: 'Route',
      component: Route
    }
  ]
})
